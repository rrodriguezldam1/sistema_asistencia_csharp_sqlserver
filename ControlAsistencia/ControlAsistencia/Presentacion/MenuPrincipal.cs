﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ControlAsistencia.Presentacion
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();
        }
        private void MenuPrincipal_Load(object sender, EventArgs e)
        {
            panelBienvenida.Dock = DockStyle.Fill; 
        }

        private void btnPersonal_Click(object sender, EventArgs e)
        {
            Personal personal = new Personal();
            panel2.Controls.Clear();
            personal.Dock = DockStyle.Fill;
            panel2.Controls.Add(personal); 
        }
    }
}
